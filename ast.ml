(* ce fichier contient le type des ast de notre langage.
   Il introduit donc les étiquettes abstraites.
 *)
  
type ast =
| Const of int
| Plus of ast * ast
