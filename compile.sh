#!/usr/bin/env bash
ocamlc -c ast.ml
menhir --infer expr.mly
ocamlc -c expr.mli
ocamlc -c expr.ml
ocamllex exprlex.mll
ocamlc -c exprlex.ml
ocamlc -c monprog.ml
ocamlc -o monprog ast.cmo expr.cmo exprlex.cmo monprog.cmo
