{
open Lexing
open Expr (* Pour pourvoir retourner des tokens il faut les connaître ! l'analyseur lexical est donc défini *après* l'analyseur syntaxique *)

exception LexicalError of string
}

let entier = '-'? ['0'-'9'] ['0'-'9']*  (* expression régulière nommée dont le langage est les entiers *)
let blanc = [' ' '\t' '\n']+            (* expression régulière nommée dont le langage est les blancs *)

rule lecture =   (* définition de la composition d'automates, lequel a reconnu l'expression ?*)
  parse
  | blanc    { lecture lexbuf } (* blanc ? --> on continue la lecture à neuf *)
  | entier      { CONST (int_of_string (Lexing.lexeme lexbuf)) } (* entier ? --> token CONST étiqueté par un  int *)
  | '+'      { PLUS } (* caractère "croix grecque" ? --> token PLUS *)
  | _ { raise (LexicalError ("Caractere inattendu: " ^ Lexing.lexeme lexbuf)) }
  | eof      { EOF } (* enfin le cas spécial de la fin de fichier *)
