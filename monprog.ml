(* Le fichier principal est bien sûr celui qui appelle les analyseurs
   sur le fichier texte passé en argument.
   
   Il a donc besoin :

   - des types définissant l'arbre de syntaxte abstraite
   - de l'analyseur lexical, ici exprlex,
   - de l'analyseur syntaxique, ici expr.

   Le résultat de l'analyse est une option (on en sait pas a priori si
   le fichier analysé était correctement écrit *)


open Ast
open Lexing
open Printf


let rec compte_cst a = 
  (* Ici il faut changer le 0 pour renvoyer le nombre de constantes de
  l'expression *)
  0 

let rec eval_expr a = 
  (* Coder ici l'évaluation de l'expression *)
 failwith "not implemented"

let print_position outx lexbuf =
  let pos = lexbuf.lex_curr_p in
  fprintf outx "%s:%d:%d" pos.pos_fname
    pos.pos_lnum (pos.pos_cnum - pos.pos_bol + 1)

let parse_with_error lexbuf =
  try Expr.content Exprlex.lecture lexbuf with
  | Exprlex.LexicalError msg ->
    fprintf stderr "%a: %s\n" print_position lexbuf msg;
    None
  | Expr.Error ->
    fprintf stderr "%a: syntax error\n" print_position lexbuf;
    exit (-1)

let _ = 
  let in_ch = Lexing.from_channel stdin in
  match parse_with_error in_ch with (* on analyse ici *)
  | None -> printf "Rien compris"   
  | Some expr -> printf "%s\n nb. cst : %s\n valeur : %s\n" "J'ai trouve" (string_of_int (compte_cst expr)) (string_of_int (eval_expr expr)) (* on fait qqchose du résultat *)
