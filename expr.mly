%token <int> CONST
%token MULT
%token PARD
%token PARG
%token PLUS
%token EOF

%start <Ast.ast option> content
%%

content:
    | EOF { None }
    | e = expradd EOF { Some e }
;

expradd:
    | e1 = expradd PLUS e2 = exprpar { Ast.Plus (e1,e2)}
    | e = exprpar { e }
;

exprpar:
    | n = CONST { Ast.Const n }
;
