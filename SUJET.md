# LIFLF - TP Parsing

> IL EST INDIPENSABLE DE CONFIGURER VSCODE COMME INDIQUÉ SUR LA PAGE DE
> M. COQUERY --> ONGLET ENSEIGNEMENT --> PROGRAMMATION FONCTIONNELLE -->
> PREMIER LIEN SUR LA PAGE "[Configuration pour la salle de TP](https://forge.univ-lyon1.fr/programmation-fonctionnelle/lifpf/-/blob/main/CONFIGURATION.md)".

Le dossier est constitué

- d'une déclaration du type de l'arbre de syntaxe abstraite (`ast.ml`)
- d'une définition de la grammaire avec les actions associées (`expr.mly`)
- d'une définition des automates qui composeront le transducteur final
  (`exprlex.mll`)
- d'un fichier principal, enfin, qui lance l'analyse de ce qui est mis
  sur l'entrée standard.

Le dossier fournit aussi un script permettant de compiler et lier le
tout pour créer un exécutable.

On se propose de coder un petit compilateur qui retournera, sur
l'entrée d'une expression artihmétique simple, l'arbre de syntaxe
abstraite associé. On pourra alors chercher à donner une sémantique à
cet arbre (évaluation, etc.)

> Les fichiers proposés en exemple proposent un langage très simple où
> seuls les entiers et les additions (assoc. à gauche) sont reconnus.
> Le but du TP est d'étendre le langage reconnu à ce qui a été traité en
> cours : additions, multiplications, constantes et variables.

0. Exécuter le script de compilation et tester l'exécutable obtenu sur quelques
   expressions simples, comme 3 ou 3 + 4. Attention, il faudra peut-être dire au
   programme que l'entrée est finie en utilisant `Ctrl+D`.

1. Le fichier `.mly` proposé commence par déclarer comme
   tokens les constructeurs du type `ast`, éventuellement avec le type
   de leur étiquetage (regarder `CONST`).
   Ces déclarations sont suivies de la désignation de la source de la
   grammaire (le `start`) et le type des valeurs pour cette sorte.
   On retourne ici une `option` sur un ast pour un enracinement jusqu'à
   la source `start`.
   Suivent enfin les règles de grammaire accompagnées de leurs
   actions. `content` est ici un NON terminal, il produit ou bien le
   token spécial `EOF` pour la fin de fichier, ou bien le NON terminal
   e de sorte expr suivi de `EOF`. Les actions des enracinements sont
   entre accolades, comme en cours (et comme dans la plupart des
   outils de ce type). Ici c'est l'outil `menhir` qui est utilisé pour
   construire l'automate à pile réalisant l'analyse ascendante à
   partir de la description de votre grammaire.

   - Étendre le type `ast` pour prendre en compte les multiplications.

   - Étendre la grammaire décrite par le fichier `.mly` pour prendre en compte
     les multiplications (prioritaire sur l'addition, assoc. gauche) et les
     expressions parenthésées. Les tokens supplémentaires utiles pour cela sont
     déjà définis : `MULT`, `PARD` (parenthèse droite) et `PARG` (parenthèse
     gauche).

2. Le fichier `.mll` contient la définition de la composition des
   transducteurs pour l'analyse lexicale. Il déclare deux expressions
   régulières entier et blanc dont les langages sont respectivement
   les entiers et les blancs. Il définit ensuite le transducteur qui,
   suivant l'expression reconnue, retourne le token adapté.
   On observera le fichier `.ml` généré à partir de ce fichier par
   l'outil `ocamllex` qui construit effectivement le transducteur décrit par
   le `.mll` (c'est horrible mais au moins ce n'est pas à vous de le
   faire, et vous savez le faire).

   - Étendre ce fichier pour prendre en compte les multiplications
     (désignées en syntaxe utilisateur par le caractère '\*') et les parenthèses.

3. Le fichier `monprog` contient les appels aux analyseurs sur l'entrée
   standard. On voit que le traîtement final ne fait qu'un affichage
   sommaire suivant que l'`option ast` est `None` ou non.

   - Coder la fonction `compte_cst` qui retourne le nombre de constantes d'un ast
     passé en paramètre.
   - Tester cette fonction sur des expressions saisies sur l'entrée
     standard.

   - Coder la fonction `eval_expr` qui retourne le résultat de l'évaluation d'une
     expression représentée par un ast, où `PLUS` est associé à l'addition
     des int, `MULT` est associé à la multiplication des int.
   - Tester cette fonction sur des expressions saisies sur l'entrée
     standard.

4. Étendre les expressions reconnues aux expressions avec nommage (par
   "let ...=... in ... " et variables). Nous les évaluerons au prochain TP.
